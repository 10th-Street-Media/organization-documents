# OPERATING AGREEMENT

## for 10th Street Media, LLC,

## a single-member-managed limited liability company in Indiana

### Article 1 - Company Formation

1.1 **Formation.** The Member-Manager hereby forms a single-member-managed limited liability company (the Company) in the State of Indiana, in accordance with Indiana Code 23-18. Articles of Organization will be filed with the office of the Indiana Secretary of State.

1.2 **Name.** The official name of the Company will be 10th Street Media, LLC.

1.3 **Registered Agent.** The name and location of the Company’s registered agent will be:
       Michael Hawkes
	   1115 King Ave.
	   Indianapolis, IN 46222

1.4 **Term.** The Company will continue in perpetuity unless one or more of the following occurs:

a) The Member-Manager votes for dissolution.  
b) The Member-Manager dies or becomes incapacitated.  
c) Any event which makes it unlawful for the Company to be carried on by the Member-Manager.  
d) Any other event which may cause dissolution of a Limited Liability Company in the State of Indiana.  

1.5 **Business Purpose.** The purpose of the company if to provide individuals and small businesses with reasonably priced Information Technology (IT) services.

1.6 **Principal Place of Business.** The location of the company’s principal place of business will be: 1115 King Avenue, Indianapolis, Indiana 46222. The principal place of business may be changed to a location that the Member-Manager may select. The Member-Manager may also choose to store company documents at a different location.

1.7 **Member.** The name and place of residence of the Member-Manager are contained in Exhibit 1 attached to this Agreement.

1.8 **Admission of Additional Members.** Except as otherwise expressly provided in this agreement, additional members may be admitted to the Company through issuance by the company of a new interest in the Company or a sale of current a percent of current Member-Manager’s interest.

### Article 2 - Capital Contributions

2.1 **Initial Contributions.** The Member-Manager will make initial contributions to the company in the form of cash and assets, as described in Exhibit 2 attached to this agreement.

2.2 **Additional Contributions.** Except as provided in Article 6.2, the Member-Manager will not be obligated to make additional contributions to the Company’s capital.

### Article 3 - Profits, Losses, and Distributions

3.1 **Profits and Losses.** For financial accounting and tax purposes, the Company’s net profits or net losses shall be determined on an annual basis, and will be allocated to individual members in proportion to each member’s relative capital interest in the Company, as set forth in Exhibit 2, and as amended from time to time in accordance with Treasury Regulation 1.704-1.

3.2 **Distributions.** The Member-Manager will determine and distribute funds annually, or at more frequent intervals, as the Member-Manager sees fit. The term “available funds” in this agreement will mean the net cash of the Company available after suitable provisions have been made for the Company’s expenses and liabilities, as determined by the Member-Manager. Distributions in liquidation of the Company or in liquidation of a member's interest shall be made in accordance with the positive capital account balances pursuant to Treasury Regulation 1.704-l(b)(2)(ii)(b)(2). To the extent a member shall have a negative capital account balance, there shall be a qualified income offset, as set forth in Treasury Regulation 1.704-l(b)(2)(ii)(d).

### Article 4 - Management
4.1 **Management of the Business.** The management of the business is vested in the Member-Manager.

4.2 **Member.** The liability of the Member-Manager will be limited as provided pursuant to the laws of the State of Indiana. The Member-Manager is in control, management, direction, and operation of the Company’s affairs and will have full powers to bind the Company with any legally binding agreement, including setting up and operating a LLC company bank account.

4.3 **Powers of the Member-Manager.** In accordance with Article 4.2 of this agreement, only the Member-Manager is authorized to make all decisions on the Company’s behalf as to

a) the sale, development, lease, or other disposition of the Company’s assets;  
b) the purchase or acquisition of other assets of any kind;  
c) the management of any or all parts of the Company’s assets;  
d) the borrowing of money and the granting of security interests in the Company’s assets;  
e) the pre-payment, refinancing or extension of any loan affecting the Company's assets;  
f) the compromise or release of any of the Company's claims or debts; and,  
g) the employment of persons, firms or corporations for the operation and management of the company's business.

In the exercise of its management powers, the Member-Manager is authorized to execute and deliver:

a) all contracts, conveyances, assignments leases, sub-leases, franchise agreements, licensing agreements, management contracts and maintenance contracts covering or affecting the Company's assets;  
b) all checks, drafts and other orders for the payment of the Company's funds;  
c) all promissory notes, loans, security agreements and other similar documents; and,  
d) all other instruments of any other kind relating to the Company's affairs, whether like or unlike the foregoing.

4.4 **Nominee.** Title of the Company’s assets will be held in the Company’s name or the name of any nominee as the Member-Manager may designate. The Member-Manager will have the power to enter into a nominee agreement with any such person, and such agreement may contain provisions indemnifying the nominee, except for the nominee’s misconduct.

4.5 **Company Information.** Upon request, the Member-Manager will supply to any other member information regarding the Company or its activities. Each member or their authorized representative shall have access to and may inspect and copy all books, records and materials in the Member-Manager’s possession regarding the Company or its activities.

4.6 **Exculpation.** The Member-Manager will not be be subjected to any liability by the Company, when by any act or omission of the Member-Manager, the effect of which may cause or result in loss or damage to the Company or the Member-Manager, if it was done in good faith to promote the best interests of the Company.

4.7 **Indemnification.** Aside from activities, suits, or proceedings initiated by the Company or the Member-Manager, the Company will indemnify any person who was or is a party defendant, or is threatened to be made a party defendant, in a pending or completed action, suit, or proceeding, whether civil, criminal, administrative, or investigative, by reason of the fact that they or were a member, Member-Manager, employee, or agent of the Company, or are or were serving at the request of the Company, for any instant expenses (including attorney fees), judgments, fines, and amounts paid into settlement actually and reasonably incurred in connection with such action, suit or proceeding if the aforementioned person acted in good faith and in a manner they reasonably believed to be in the best interest of, or not opposed to the best interest of the Company, and with respect to any criminal action proceeding, they had no reasonable cause to believe their conduct was unlawful. The termination of any action, suit, or proceeding by judgment, order, settlement, conviction, or upon a plea of "no lo Contendere" or its equivalent, will not in itself create a presumption that the person did or did not act in good faith and in a manner which they reasonably believed to be in the best interest of the Company, and, with respect to any criminal action or proceeding, had reasonable cause to believe that their conduct was lawful.

4.8 **Records.** The Member-Manager will cause the Company to keep the following records at its primary place of business, or any other location as the Member-Manager may designate:

a) A copy of the Certificate of Formation, and the Company's Operating Agreement, plus all amendments:  
b) Copies of the Company’s federal, state, and local income tax returns and reports, if any, for the past three years:  
c) Copies of any financial statements of the limited liability company for the past three years.

#### Article 5 - Compensation
5.1 **Member Management Fee.** Any member rendering services to the company will be entitled to compensation commensurate with the value of such services, provided they have written authorization from the Member-Manager to render such services.

5.2 **Reimbursement.** The Company shall the Member-Manager for any out-of-pocket expenses incurred by the Member-Manager in managing the company.

### Article 6 - Bookkeeping
6.1 **Books.** The Member-Manager will maintain complete and accurate books of account of the Company’s affairs at the Company's principal place of business or other designated location. Such books will be kept on such method of accounting as the Member-Manager will direct. The company’s accounting period will be the calendar year.

6.2 **Member’s Accounts.** The Member-Manager will maintain separate capital and distribution accounts for each member. Each member’s capital accounts shall be determined and maintained in a manner set forth in Treasury Regulation 1.704-l(b)(2)(iv) and will consist of the member’s initial capital contribution increased by:

a) Any additional contribution made by the member;  
b) Credit balances transferred from the member’s distribution account to their credit account;

and decreased by:

a) Distributions to the member in reduction of the Company capital;  
b) The member’s share of any Company losses if charged into the member’s account.

6.3 **Reports.** The Member-Manager will close the books at the end of each calendar year, and will prepare and send to each member a statement of the member’s distributive share of income and expenses for income tax reporting purposes.

### Article 7 - Transfers

7.1 **Assignment.** According to the appropriate Court, should any member have a creditor with a judgment which was issued an assignment of the membership interest, the creditor will only obtain an assignment of membership interest, not an actual transfer of membership in the LLC. The new assignee will not have any rights of membership in the LLC and will not be able to participate in management of the LLC, and will not be able to dissolve the LLC. The new assignee is only granted rights to the distributions of a member’s interests, if the Member-Manager decides to distribute at all, not the rights of membership. The assignee must release the member’s interests back to the member upon payment of the judgment in accordance with the appropriate court.

### Article 8 - Dissolution

8.1 **Dissolution.** The Member-Manager may dissolve the Company at any time. The Member-Manager may NOT dissolve the Company for a loss of membership interests. Upon dissolution the Company must first pays its debts before distributing cash, assets, and/or initial capital to the Member-Manager’s interests or the interests of any other members. The dissolution may only be ordered by the Member-Manager, and not by the interests of any other members.

### Article 9 - Technology
9.1 **Paperless Office.** The Company will strive to maintain a paperless office whenever possible. No documents will be printed unless absolutely necessary, and electronic documents will be preferred to paper documents.

a) Paper documents which are delivered to the Company will be scanned into PDF format.  
b) All electronic documents for the Company will be stored on the Member-Manager’s personal computer, which will be backed up on a daily basis.

9.2 **Backups and Recovery.** The Member-Manager’s personal computer will be backed up on a daily basis. The system will be tested once a month to ensure files can be recovered.

a) Each website maintained by the Company will have a backup and recovery procedure in place.




## Certificate of Formation
This Company Operating Agreement is entered into and will become effective as of the Effective Date by and among the Company and the person executing this Agreement as Member-Manager. It is the Member-Manager’s express intention to create a limited liability company in accordance with currently applicable laws in the State of Indiana.   

The undersigned hereby agrees, acknowledges, and certifies that the foregoing agreement of six pages, and that along with Exhibits 1 and 2, constitutes the Operating Agreement of 10th Street Media, LLC, and is adopted and approved by each member on September __, 2017.




Member-Manager signature




Printed name




Percent




## Exhibit 1
### Listing of members
As of September  ___, 2017, the following is a list of members of the Company.

| Name | Address | Percent |
| --- | --- | ---:|
| Michael Hawkes | 1115 King Avenue  Indianapolis, IN 46222  | 100% |




## Exhibit 2
### Capital Contributions
Pursuant to Article 2,  the Member-Manager’s initial contribution to the Company capital is stated to be $_______________. The description and each individual portion of this initial contribution are as follows.

[list containing descriptions of contributions and amounts]
